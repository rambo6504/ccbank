local accountFolderPath = "./bankSys/accountSys/accounts"
local currentAccounts = fs.list(accountFolderPath)
local blankAccount =
{
    ["Name"] = "",
    ["Money"] = 0
}
function makeAccount(name)
    local newAc = blankAccount
    newAc.Name = name
    local playerFile = fs.open(accountFolderPath..name,"w")
    local package = textutils.serialise(newAc)
    playerFile.write(package)
    playerFile.close()
end